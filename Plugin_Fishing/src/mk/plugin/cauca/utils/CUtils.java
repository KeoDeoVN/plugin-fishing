package mk.plugin.cauca.utils;

import java.time.LocalTime;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import mk.plugin.cauca.config.ConfigValue;
import mk.plugin.cauca.fish.CFish;

public class CUtils {
	
	public static void broadcast(String message) {
		Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(message.replace("&", "§")));
	}
	
	public static void broadcast(List<String> messages) {
		Bukkit.getOnlinePlayers().forEach(player -> {
			messages.forEach(message -> {
				broadcast(message.replace("&", "§"));
			});
		});
	}
	
	public static boolean isInPeriod(LocalTime start, LocalTime end) {
		LocalTime now = LocalTime.now();
		return now.isAfter(start) && now.isBefore(end);
	}
	
	public static double random(double min, double max) {
		return (new Random().nextInt(new Double((max - min) * 1000).intValue()) + min * 1000) / 1000;
	}
	
	public static void broadcast(String m, Sound sound) {
		Bukkit.getOnlinePlayers().forEach(player -> {
			player.sendMessage(m.replace("&", "§"));
			if (sound != null) {
				player.playSound(player.getLocation(), sound, 1, 1);
			}
		});
	}
	
	public static void broadcast(List<String> messages, Sound sound) {
		Bukkit.getOnlinePlayers().forEach(player -> {
			messages.forEach(message -> {
				broadcast(message.replace("&", "§"));
			});
			if (sound != null) {
				player.playSound(player.getLocation(), sound, 1, 1);
			}
		});
	}
	
	public static void broadcastTitle(String title, String subtitle, int a, int b, int c) {
		Bukkit.getOnlinePlayers().forEach(player -> {
			player.sendTitle(title, subtitle, a, b, c);
		});
	}
	
	public static boolean rate(double chance) {
		if (chance >= 100) return true;
		double rate = chance * 100;
		int random = new Random().nextInt(10000);
		if (random < rate) {
			return true;
		} else return false;
	}
	
	public static void set(ItemStack from, ItemStack to) {
		from.setType(to.getType());
		from.setItemMeta(to.getItemMeta());
	}
	
	public static CFish rate() {
		// Get max
		CFish fish = (CFish) ConfigValue.FISHES.values().toArray()[0];
		for (CFish f : ConfigValue.FISHES.values()) {
			if (f.getChance() > fish.getChance()) fish = f;
		}
		
		// Rate
		for (CFish f : ConfigValue.FISHES.values()) {
			if (rate(f.getChance())) fish = f;
		}
		
		return fish;
	}
	
}

package mk.plugin.cauca.utils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import mk.plugin.cauca.config.ConfigValue;

public class ScoreUtils {
	
	public static Map<String, Map<String, Integer>> Scores = Maps.newHashMap();
	
	public static int getScore(Player player, String type) {
		if (!Scores.containsKey(player.getName())) return 0;
		return Scores.get(player.getName()).getOrDefault(type, 0);
	}
	
	public static void setScore(Player player, String type, int score) {
		Map<String, Integer> m = Scores.getOrDefault(player.getName(), Maps.newHashMap());
		m.put(type, score);
		Scores.put(player.getName(), m);
	}
	
	public static void addScore(Player player, String type, int amount) {
		setScore(player, type, getScore(player, type) + amount);
	}
	
	public static Map<String, Integer> getTop(String type) {
		Map<String, Integer> players = Maps.newLinkedHashMap();
		Scores.forEach((name, m) -> {
			players.put(name, m.getOrDefault(type, 0));
		});
		
		Map<String, Integer> result = players.entrySet()
                .stream()
                .sorted((Map.Entry.<String, Integer>comparingByValue().reversed()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		
		return result;
	}
	
	public static String getTop(String type, int top) {
		Map<String, Integer> players = getTop(type);
		if (players.size() < top) return null;
		int c = 0;
		for (String n : players.keySet()) {
			c++;
			if (c == top) return n;
		}
		return null;
	}
	
	public static void rewards() {
		ConfigValue.REWARDS.forEach((fish, rewards) -> {
			int top = 0;
			for (String cmds : rewards) {
				top++;
//				if (top > 10) break;
				String name = getTop(fish, top);
				if (name == null) break;
				for (String cmd : cmds.split(";")) {
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("%player%", name));
				}
			}
		});
	}
	
	public static void broadcastTop() {
		ConfigValue.FISHES.forEach((id, fish) -> {
			Map<String, Integer> players = getTop(id);
			int count = 0;
			CUtils.broadcast("");
			CUtils.broadcast("§2§lTop cá " + ItemStackUtils.getName(fish.getItemStack()));
			for (Entry<String, Integer> e : players.entrySet()) {
				count++;
				CUtils.broadcast("§aTop " + count + ". §f" + e.getKey() + " " + e.getValue());
				if (count >= ConfigValue.TOP_ANNOUCEMENT) break;
			}
			CUtils.broadcast("");
		});
	}
	
}

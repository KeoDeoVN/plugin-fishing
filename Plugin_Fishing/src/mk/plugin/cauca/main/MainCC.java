package mk.plugin.cauca.main;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import mk.plugin.cauca.config.ConfigValue;
import mk.plugin.cauca.listener.CListener;
import mk.plugin.cauca.task.TimeCheckingTask;
import mk.plugin.cauca.utils.CUtils;
import mk.plugin.cauca.utils.ScoreUtils;

public class MainCC extends JavaPlugin {
	
	public static MainCC plugin;
	
	public FileConfiguration config;
	
	@Override
	public void onEnable() {
		plugin = this;
		this.saveDefaultConfig();
		this.reloadConfig();
		
		this.getCommand("cauca").setExecutor(new Commands());
		
		Bukkit.getPluginManager().registerEvents(new CListener(), this);
		
		// Start
		new TimeCheckingTask(this, ConfigValue.START.toSecondOfDay() * 1000, 86400000, () -> {
			CUtils.broadcast(ConfigValue.START_BROADCAST, ConfigValue.START_SOUND);
			ScoreUtils.Scores.clear();
		});
		
		// End
		new TimeCheckingTask(this, ConfigValue.END.toSecondOfDay() * 1000, 86400000, () -> {
			CUtils.broadcast(ConfigValue.END_BROADCAST, ConfigValue.END_SOUND);
			ScoreUtils.rewards();
		});
	}
	
	public void reloadConfig() {
		config = YamlConfiguration.loadConfiguration(new File(this.getDataFolder(), "config.yml"));
		ConfigValue.init(config);
	}

}

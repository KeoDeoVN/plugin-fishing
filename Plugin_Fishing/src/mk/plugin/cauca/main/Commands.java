package mk.plugin.cauca.main;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.cauca.config.ConfigValue;

public class Commands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		if (!sender.hasPermission("cc.*")) return false;
		
		if (args.length == 0) {
			sendTut(sender);
			return false;
		}
		
		if (args[0].equalsIgnoreCase("reload")) {
			MainCC.plugin.reloadConfig();
			sender.sendMessage("Reloaded!");
		}
		
		else if (args[0].equalsIgnoreCase("getitems")) {
			ConfigValue.FISHES.values().forEach(fish -> {
				((Player) sender).getInventory().addItem(fish.getItemStack());
			});
		}
		
		return false;
	}
	
	public void sendTut(CommandSender sender) {
		sender.sendMessage("§a/cauca reload: Reload config");
		sender.sendMessage("§a/cauca getitems: Lấy tất cả các loại cá");
	}

}

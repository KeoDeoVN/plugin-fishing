package mk.plugin.cauca.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import mk.plugin.cauca.config.ConfigValue;
import mk.plugin.cauca.fish.CFish;
import mk.plugin.cauca.main.MainCC;
import mk.plugin.cauca.utils.CUtils;
import mk.plugin.cauca.utils.ItemStackUtils;
import mk.plugin.cauca.utils.ScoreUtils;

public class CListener implements Listener {
	
	@EventHandler
	public void onFishing(PlayerFishEvent e) {
		if (!CUtils.isInPeriod(ConfigValue.START, ConfigValue.END)) return;
		if (!CUtils.rate(ConfigValue.GET_CHANCE)) return;
		
		if (e.getState() != State.CAUGHT_FISH) return;
		
		Item item = (Item) e.getCaught();
		if (item == null) return;
		
		Player player = e.getPlayer();
		item.setPickupDelay(40);
		CFish fish = CUtils.rate(); 
		
		ItemStack itemStack = fish.getItemStack();
		item.setItemStack(itemStack);
		
		player.playSound(player.getLocation(), ConfigValue.GET_SOUND, 1, 1);
		CUtils.broadcast(ConfigValue.GET_BROADCAST.replace("%player%", player.getName()).replace("%name%", ItemStackUtils.getName(itemStack)), null);
		
		ScoreUtils.addScore(player, fish.getID(), 1);
		
		Bukkit.getScheduler().runTask(MainCC.plugin, () -> {
			Vector v = item.getVelocity().multiply(0.8);
			v = v.setX(v.getX() * (1 + CUtils.random(-0.7, 0.7)));
			v = v.setY(v.getY() * (1 + CUtils.random(0.1, 0.7)));
			v = v.setZ(v.getZ() * (1 + CUtils.random(-0.7, 0.7)));		
			item.setVelocity(v);
		});

	}
	
}

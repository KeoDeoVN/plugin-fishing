package mk.plugin.cauca.fish;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

import kdvn.facrpg.shopitemgui.itemdatabase.ItemManager;
import mk.plugin.niceshops.storage.ItemStorage;

public class CFish {
	
	private String id;
	private double chance;
	private String item;
	
	public CFish(String id, double chance, String item) {
		this.id = id;
		this.chance = chance;
		this.item = item;
	}
	
	public String getID() {
		return this.id;
	}
	
	public double getChance() {
		return this.chance;
	}
	
	public ItemStack getItemStack() {
		if (Bukkit.getPluginManager().isPluginEnabled("Shops")) {
			ItemManager.getItem(this.item).clone();
		}
		else if (Bukkit.getPluginManager().isPluginEnabled("NiceShops")) {
			ItemStorage.get(this.item);
		}
		return null;
	}
	
}

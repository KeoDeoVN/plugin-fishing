package mk.plugin.cauca.config;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;

import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.cauca.fish.CFish;

public class ConfigValue {
	
	public static LocalTime START;
	public static LocalTime END;
	public static List<LocalTime> ANNOUNCEMENTS = Lists.newArrayList();
	
	public static List<String> START_BROADCAST;
	public static List<String> END_BROADCAST;
	public static String GET_BROADCAST;
	
	public static Sound START_SOUND;
	public static Sound END_SOUND;
	public static Sound GET_SOUND;
	
	public static int TOP_ANNOUCEMENT;
	
	public static Map<String, CFish> FISHES = Maps.newHashMap();
	
	public static Map<String, List<String>> REWARDS = Maps.newHashMap();
	
	public static int GET_CHANCE = 100;
	
	public static void init(FileConfiguration config) {
		START = LocalTime.parse(config.getString("time.start"));
		END = LocalTime.parse(config.getString("time.end"));
		ANNOUNCEMENTS.clear();
		config.getStringList("time.announcement").forEach(s -> {
			ANNOUNCEMENTS.add(LocalTime.parse(s));
		});
		
		START_BROADCAST = config.getStringList("message.start-broadcast");
		END_BROADCAST = config.getStringList("message.end-broadcast");
		GET_BROADCAST = config.getString("message.get-broadcast");
		
		START_SOUND = config.contains("sound.start") ? Sound.valueOf(config.getString("sound.start")) : null;
		END_SOUND = config.contains("sound.end") ? Sound.valueOf(config.getString("sound.end")) : null;
		GET_SOUND = config.contains("sound.get") ? Sound.valueOf(config.getString("sound.get")) : null;
		
		FISHES.clear();
		config.getConfigurationSection("fish").getKeys(false).forEach(id -> {
			double chance = config.getDouble("fish." + id + ".chance");
			String item = config.getString("fish." + id + ".item");
			FISHES.put(id, new CFish(id, chance, item));
		});
		
		REWARDS.clear();
		config.getConfigurationSection("top.reward").getKeys(false).forEach(id -> {
			REWARDS.put(id, config.getStringList("top.reward." + id));
		});
		
		TOP_ANNOUCEMENT = config.getInt("time.top");
		
		GET_CHANCE = config.getInt("get-chance");
	}
	
}
